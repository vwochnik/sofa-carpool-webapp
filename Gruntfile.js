module.exports = function(grunt) {

  var path = require('path');
  var matchdep = require('matchdep');

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    cfg: grunt.file.readJSON("config.json"),
    meta: {
      sourceDir: "src",
      lessDir: "less",
      scriptDir: 'scripts',
      htmlIncludeDir: "include",
      partialDir: "partials",
      buildDir: "dist",
      tempDir: "tmp",
      staticFiles: ['**/*.{txt,ico,php}', '**/.htaccess', 'assets/*.*'],
      lessFiles: "**/*.less",
      htmlFiles: '**/*.html',
      imageFiles: '**/*.{jpg,jpeg,png,gif}',
      xmlFiles: '**/*.{xml,svg}'
    }
  });

  grunt.config('clean', {
    build: {
      src: ['<%= meta.buildDir %>/**']
    },
    html: {
      src: ['<%= meta.tempDir %>/html/**']
    },
    assets: {
      src: ['<%= meta.tempDir %>/assets/**']
    }
  });

  grunt.config('less', {
    build: {
      options: {
        paths: '<%= cfg.lessDirectories %>',
        compress: true
      },
      files: [{
        expand: true,
        cwd: '<%= meta.sourceDir %>',
        src: '<%= meta.lessFiles %>',
        dest: '<%= meta.buildDir %>',
        filter: 'isFile',
        rename: function(dest, src) {
            return dest+'/' + src.substring(0, src.lastIndexOf('.'))
                 + '.min.css';
        }
      }]
    }
  });

  grunt.config('uglify', {
    options: {
      preserveComments: 'some'
    },
    build: {
      files: {
        '<%= meta.buildDir %>/assets/js/application.min.js':
            '<%= cfg.applicationScripts %>',
        '<%= meta.buildDir %>/assets/js/modules.min.js':
            '<%= cfg.moduleScripts %>'
      }
    }
  });

  grunt.config('concat', {
    jsDependencies: {
      files: {
        '<%= meta.buildDir %>/assets/js/dependencies.min.js':
            '<%= cfg.jsDependencies %>'
      }
    },
    develop: {
      files: {
        '<%= meta.buildDir %>/assets/js/application.min.js':
            '<%= cfg.applicationScripts %>',
        '<%= meta.buildDir %>/assets/js/modules.min.js':
            '<%= cfg.moduleScripts %>'
      }
    }
  });

  grunt.config('html2js', {
    options: {
      base: '',
      useStrict: true,
      module: "spPartials",
      singleModule: true,
      quoteChar: '\'',
      htmlmin: {
        collapseBooleanAttributes: false,
        collapseWhitespace: true,
        removeAttributeQuotes: false,
        removeComments: true,
        removeEmptyAttributes: false,
        removeRedundantAttributes: false,
        removeScriptTypeAttributes: false,
        removeStyleLinkTypeAttributes: false
      }
    },
    build: {
      src: ['<%= meta.partialDir %>/**/*.html'],
      dest: '<%= meta.buildDir %>/assets/js/partials.min.js'
    }
  });

  grunt.config('copy', {
    static: {
      files: [{
        expand: true,
        cwd: '<%= meta.sourceDir %>',
        src: '<%= meta.staticFiles %>',
        dest: '<%= meta.buildDir %>',
        filter: 'isFile'
      }]
    },
    assets: {
      files: [{
        expand: true,
        cwd: '<%= meta.tempDir %>/assets',
        src: '**',
        dest: '<%= meta.buildDir %>',
        filter: 'isFile'
      }]
    },
    glyphicons: {
      files: [{
        expand: true,
        cwd: 'components/bootstrap/fonts',
        src: ['**'],
        dest: '<%= meta.buildDir %>/assets/fonts/'
      }]
    }
  });

  grunt.config('imagemin', {
    assets: {
      options: {
        optimizationLevel: 7
      },
      files: [{
        expand: true,
        cwd: '<%= meta.sourceDir %>',
        src: '<%= meta.imageFiles %>',
        dest: '<%= meta.tempDir %>/assets',
        filter: 'isFile'
      }]
    }
  });

  grunt.config('connect', {
    server: {
      options: {
        port: 8080,
        keepalive: false,
        livereload: true,
        open: true,
        base: '<%= meta.buildDir %>'
      }
    }
  });

  grunt.config('preprocess', {
    html: {
      files: [{
        expand: true,
        cwd: '<%= meta.sourceDir %>',
        src: '<%= meta.htmlFiles %>',
        dest: '<%= meta.tempDir %>/html',
        filter: 'isFile'
      }]
    }
  });

  grunt.config('htmlmin', {
    build: {
      options: {
        removeComments: true,
        collapseWhitespace: true
      },
      files: [{
        expand: true,
        cwd: '<%= meta.tempDir %>/html',
        src: '<%= meta.htmlFiles %>',
        dest: '<%= meta.buildDir %>',
        filter: 'isFile'
      }]
    }
  });

  grunt.config('xmlmin', {
    assets: {
      files: [{
        expand: true,
        cwd: '<%= meta.sourceDir %>',
        src: '<%= meta.xmlFiles %>',
        dest: '<%= meta.tempDir %>/assets/',
        filter: 'isFile'
      }]
    }
  });

  grunt.config('watch', {
    source: {
      options: {
        livereload: true
      },
      files: ['<%= meta.sourceDir %>/**/*',
              '<%= meta.lessDir %>/**/*',
              '<%= meta.scriptDir %>/**/*',
              '<%= meta.partialDir %>/**/*',
              '<%= meta.htmlIncludeDir %>/**/*'],
      tasks: ['builddev']
    }
  });

  grunt.config('jshint', {
    options: {
      camelcase: true,
      curly: false,
      eqeqeq: true,
      unused: true,
      strict: true,
      eqnull: true,
      laxbreak: true,
      browser: true
    },
    modules: {
      options: {
          globals: {
            jQuery: false,
            google: false,
            angular: false
          }
      },
      files: {
        src: '<%= cfg.moduleScripts %>'
      }
    }
  });

  grunt.config('exec', {
    deploy: './utils/deploy.sh'
  });

  grunt.registerTask('assets', [
    'newer:imagemin:assets',
    'newer:xmlmin:assets',
    'copy:assets'
  ]);

  grunt.registerTask('html', [
    'clean:html',
    'preprocess:html',
    'htmlmin:build'
  ]);

  grunt.registerTask('build', [
    'clean:build',
    'copy:static',
    'less:build',
    'concat:jsDependencies',
    'uglify:build',
    'html2js:build',
    'assets',
    'copy:glyphicons',
    'html'
  ]);

  grunt.registerTask('builddev', [
    'clean:build',
    'copy:static',
    'less:build',
    'concat:jsDependencies',
    'concat:develop',
    'html2js:build',
    'assets',
    'copy:glyphicons',
    'html'
  ]);

  grunt.registerTask('server', ['connect:server', 'watch:source']);
  grunt.registerTask('develop', ['builddev', 'server']);
  grunt.registerTask('test', ['jshint:modules']);

  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);
};
