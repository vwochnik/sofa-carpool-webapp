/*!
 * listing.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spListing', ['$q', 'spIdentity', 'spBase',
    function($q, spIdentity, spBase) {
      function getListings() {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          spBase.get('/getAllListingsFromUser/'+id).then(function(response) {
            var listings = [];
            ng.forEach(response, function(value) {
              listings.push(convertResponse(value));
            });
            deferred.resolve(listings);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function getListing(listingId) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(/*id*/) {
          spBase.get('/getListing/'+listingId).then(function(response) {
            deferred.resolve(convertResponse(response));
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function addListing(listing) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var data = ng.extend({}, listing, {
            userId: id,
            time: spBase.date2string(listing.time || new Date())
          });
          spBase.post('/addListing', data).then(function(response) {
            if (response.listingId > 0) {
              deferred.resolve(convertResponse(response));
            } else {
              deferred.reject("Listing creation failed.");
            }
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function deleteListing(/*listingId*/) {
        var deferred = $q.defer();

        //TODO: impl
        deferred.reject("Not possible at the moment.");

        return deferred.promise;
      }

      function convertResponse(listing) {
        return ng.extend({}, listing, {
          time: spBase.string2date(listing.time||''),
          created: spBase.string2date(listing.created||'')
        });
      }

      return {
        getListings: getListings,
        getListing: getListing,
        addListing: addListing,
        deleteListing: deleteListing
      };
    }]);
})(angular);
