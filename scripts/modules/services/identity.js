/*!
 * identity.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spIdentity', ['$rootScope', '$q', 'spBase', 'spIdentityPersistence', '$timeout',
    function($rootScope, $q, spBase, spIdentityPersistence, $timeout) {
      var theIdentity, theDeferred;

      function logIn(username, password, remember) {
        if (ng.isDefined(theDeferred))
          return $q.reject("Already progressing.");

        var deferred = $q.defer();
        theDeferred = $q.defer();

        var loginData = {
          username: username,
          password: password
        };

        spBase.post('/login', loginData).then(function(response) {
          if (response.check === true) {
            theIdentity = {
              username: response.username,
              pcn: response.pcn,
              firstLogin: response.firstLogin,
              userId: response.userId || 0
            };
            spIdentityPersistence.saveIdentity(theIdentity, remember);
            deferred.resolve(theIdentity);
            broadcastUpdate();
          } else {
              deferred.reject("Invalid credentials!");
          }
          theDeferred = undefined;
        }, function(error) {
          deferred.reject(error);
        });

        return deferred.promise;
      }

      function logOut() {
        if (ng.isDefined(theDeferred))
          return $q.reject("Already progressing.");

        var deferred = $q.defer();
        theDeferred = deferred;

        $timeout(function() {
          theIdentity = undefined;
          spIdentityPersistence.purgeIdentity();
          deferred.resolve(theIdentity);
          broadcastUpdate();
          theDeferred = undefined;
        }, 0);

        return deferred.promise;
      }

      function loggedIn() {
        return ng.isDefined(theIdentity);
      }

      function getIdentity() {
        if (ng.isDefined(theDeferred))
          return theDeferred.promise;

        var deferred = $q.defer();
        deferred.resolve(theIdentity);
        return deferred.promise;
      }

      function getID() {
        var deferred = $q.defer();
        getIdentity().then(function(identity) {
          if (ng.isDefined(identity))
            deferred.resolve(identity.userId);
          else
            deferred.reject("No identity available.");
        }, function(reason) {
          deferred.reject(reason);
        });
        return deferred.promise;
      }

      function identity() {
          return theIdentity;
      }

      function broadcastUpdate() {
        $rootScope.$broadcast('identity:updated', theIdentity);
      }

      // load identity from storage
      (function(){
        var deferred = $q.defer();
        theDeferred = deferred;

        $timeout(function() {
          theIdentity = spIdentityPersistence.loadIdentity();
          deferred.resolve(theIdentity);
          theDeferred = undefined;
        }, 0);
      })();

      return {
        logIn: logIn,
        logOut: logOut,
        loggedIn: loggedIn,
        getIdentity: getIdentity,
        getID: getID,
        identity: identity
      };
    }]);
})(angular);
