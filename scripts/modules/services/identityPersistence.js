/*!
 * identityPersistence.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spIdentityPersistence', ['$localStorage', '$sessionStorage',
    function($localStorage, $sessionStorage) {
      function loadIdentity() {
        if (ng.isDefined($sessionStorage.identity))
          return $sessionStorage.identity;

        if (ng.isDefined($localStorage.identity))
          return $localStorage.identity;

        return undefined;
      }

      function saveIdentity(identity, remember) {
        $sessionStorage.identity = identity;
        if (remember)
          $localStorage.identity = identity;
      }

      function purgeIdentity() {
        delete $sessionStorage.identity;
        delete $localStorage.identity;
      }

      return {
        loadIdentity: loadIdentity,
        saveIdentity: saveIdentity,
        purgeIdentity: purgeIdentity
      };
    }]);
})(angular);

