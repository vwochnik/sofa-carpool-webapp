/*!
 * modal.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spModal', ['$q', '$modal',
    function($q, $modal) {
      function modalQuestion(heading, lead, notes) {
        var deferred = $q.defer();

        $modal.open({
          templateUrl: 'partials/modalQuestion.html',
          controller: 'spModalQuestionController',
          resolve: {
            heading: function() { return heading; },
            lead: function() { return lead; },
            notes: function() { return notes || []; }
          }
        }).result.then(function(value) {
          if (value)
            deferred.resolve();
          else
            deferred.reject();
        }, function() {
          deferred.reject();
        });

        return deferred.promise;
      }

      function modalAlert(heading, lead, notes) {
        var deferred = $q.defer();

        $modal.open({
          templateUrl: 'partials/modalAlert.html',
          controller: 'spModalAlertController',
          resolve: {
            heading: function() { return heading; },
            lead: function() { return lead; },
            notes: function() { return notes || []; }
          }
        }).result.finally(function() {
          deferred.resolve();
        });

        return deferred.promise;
      }

      return {
        modalQuestion: modalQuestion,
        modalAlert: modalAlert
      };
    }]);
})(angular);

