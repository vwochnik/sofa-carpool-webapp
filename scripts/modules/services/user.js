/*!
 * user.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spUser', ['$rootScope', '$q', '$timeout', 'spIdentity', 'spBase',
    function($rootScope, $q, $timeout, spIdentity, spBase) {
      var theUser, theDeferred;

      function refreshUser() {
        if (ng.isDefined(theDeferred))
          return $q.reject("Currently progressing.");

        var deferred = $q.defer();
        theDeferred = deferred;

        spIdentity.getIdentity().then(function(identity) {
          refreshUserByIdentity(identity, true);
        }, function(reason) {
          deferred.reject(reason);
          theDeferred = undefined;
        });

        return theDeferred.promise;
      }

      function refreshUserByIdentity(identity, broadcast) {
        var deferred = theDeferred;

        if ((!ng.isDefined(identity)) || (!identity.userId)) {
          if (ng.isDefined(theUser)) {
            theUser = undefined;
            if (broadcast)
              broadcastUpdate();
          }
          deferred.resolve(theUser);
          theDeferred = undefined;
          return;
        }

        spBase.get('/getProfileFromUser/'+identity.userId).then(function(response) {
          theUser = {
            firstName: response.firstname,
            lastName: response.lastname,
            birthDate: spBase.string2date(response.birthday),
            phone: response.phone,
            email: response.email,
            citizenship: response.citizenship,
            smoking: response.smoking
          };
          deferred.resolve(theUser);
          if (broadcast)
            broadcastUpdate();
          theDeferred = undefined;
        }, function(error) {
          deferred.reject(error);
          theDeferred = undefined;
        });
      }

      function updateUser(user) {
        if (ng.isDefined(theDeferred))
          return $q.reject("Currently progressing.");

        var deferred = $q.defer();
        theDeferred = deferred;

        spIdentity.getIdentity().then(function(identity) {
          updateUserByIdentity(identity, user);
        }, function(reason) {
          deferred.reject(reason);
          theDeferred = undefined;
        });

        return theDeferred.promise;
      }

      function updateUserByIdentity(identity, user) {
        var deferred = theDeferred;

        if ((!ng.isDefined(identity)) || (!identity.userId) || (!ng.isDefined(theUser))) {
          deferred.reject("Can't update!");
          theDeferred = undefined;
          return;
        }

        var newUser = ng.extend(ng.extend({}, theUser), user);
        var userData = {
          userId: identity.userId,
          birthday: spBase.date2string(newUser.birthDate),
          phone: newUser.phone,
          citizenship: newUser.citizenship,
          smoking: newUser.smoking
        };

        spBase.post('/updateUserProfile', userData).then(function(/*response*/) {
          theUser = newUser;
          deferred.resolve(theUser);
          broadcastUpdate();
          theDeferred = undefined;
        }, function(error) {
          deferred.reject(error);
          theDeferred = undefined;
        });
      }

      function getUser() {
        if (ng.isDefined(theDeferred))
          return theDeferred.promise;

        var deferred = $q.defer();
        deferred.resolve(theUser);
        return deferred.promise;
      }

      function user() {
        return theUser;
      }

      function broadcastUpdate() {
        $rootScope.$broadcast('user:updated', theUser);
      }

      // listen to events in an isolated scope to keep correct order
      $rootScope.$new(true, $rootScope).$on('identity:updated', function(event, identity) {
        if (ng.isDefined(theDeferred))
          theDeferred.reject("Identity change received.");

        theDeferred = $q.defer();
        refreshUserByIdentity(identity, true);
      });

      // load user for the first time
      (function() {
        var deferred = $q.defer();
        theDeferred = deferred;

        $timeout(function() {
          spIdentity.getIdentity().then(function(identity) {
            refreshUserByIdentity(identity, false);
          }, function(reason) {
            deferred.reject(reason);
            theDeferred = undefined;
          });
        }, 0);
      })();

      return {
        refreshUser: refreshUser,
        updateUser: updateUser,
        getUser: getUser,
        user: user
      };
    }]);
})(angular);
