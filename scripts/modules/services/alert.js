/*!
 * alert.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spAlert', ['$timeout',
    function($timeout) {
      var alerts = [];

      function addAlert(type, msg) {
        var alert = {type: type, msg: msg};

        $timeout(function() {
          removeAlert(alert);
        }, 5000);
        alerts.push(alert);
      }

      function removeAlert(alert) {
        var index = alerts.indexOf(alert);
        if (index < 0)
          return false;

        alerts.splice(index, 1);
        return true;
      }

      function getAlerts() {
        return alerts;
      }

      return {
        addAlert: addAlert,
        removeAlert: removeAlert,
        getAlerts: getAlerts
      };
    }]);
})(angular);

