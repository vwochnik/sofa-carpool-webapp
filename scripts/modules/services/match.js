/*!
 * match.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spMatch', ['$q', 'spIdentity', 'spBase',
    function($q, spIdentity, spBase) {

      function getAcceptedMatches() {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          spBase.get('/getAllAcceptedMatchesFromUser/'+id).then(function(response) {
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function getMatchesForListing(listingId) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(/*id*/) {
          spBase.get('/getAllMatchesFromListing/'+listingId).then(function(response) {
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function acceptMatch(match) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var matchData = { userId: id, matchId: match.id };
          spBase.post('/acceptMatch', matchData).then(function(response) {
            deferred.resolve(ng.extend({}, match, response));
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function declineMatch(match) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var matchData = { userId: id, matchId: match.id };
          spBase.post('/declineMatch', matchData).then(function(response) {
            deferred.resolve(ng.extend({}, match, response));
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      return {
        getAcceptedMatches: getAcceptedMatches,
        getMatchesForListing: getMatchesForListing,
        acceptMatch: acceptMatch,
        declineMatch: declineMatch
      };
    }]);
})(angular);
