/*!
 * message.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spMessage', ['$q', '$filter', 'spIdentity', 'spBase',
    function($q, $filter, spIdentity, spBase) {

      function getMessagesForMatch(match) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(/*id*/) {
          spBase.get('/getMessagesFromMatch/'+match.id).then(function(response) {
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function sendMessage(match, message) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var messageData = {userId: id, matchId: match.id, message: message};
          console.log(messageData);
          spBase.post('/sendMessage', messageData).then(function(response) {
            console.log(response);
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      return {
        getMessagesForMatch: getMessagesForMatch,
        sendMessage: sendMessage
      };
    }]);
})(angular);
