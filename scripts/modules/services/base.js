/*!
 * base.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }

  ng.module('spApp').factory('spBase', ['$q', '$http', '$timeout', 'spConstants',
    function($q, $http, $timeout, spConstants) {

      function request(method, uri, data) {
        var deferred = $q.defer();

        var opts = {method: method, url: spConstants.apiUrl+uri};
        if (ng.isDefined(data))
          opts.data = data;

        $http(opts).success(function(data /*, status, headers, config*/) {
          deferred.resolve(data);
        }).error(function(/*data, status, headers, config*/) {
          deferred.reject("HTTP API request failure.");
        });

        return deferred.promise;
      }

      function date2string(date) {
        if (!ng.isDefined(date))
          return undefined;
        return date.getFullYear() + '-' + pad(date.getMonth()+1, 2) + '-' + pad(date.getDate(), 2)
             + ' ' + pad(date.getHours(), 2) + ':' + pad(date.getMinutes(), 2) + ':' + pad(date.getSeconds(), 2)
             + '.' + pad(date.getMilliseconds(), 3);
      }

      function string2date(string) {
        var match;

        if ((!ng.isDefined(string)) || (!(match = string.match(/^(\d{4})\-(\d{2})\-(\d{2})(?:\ (\d{2}):(\d{2}):(\d{2})(?:\.(\d{3}))?)?/))))
          return undefined;

        return new Date(match[1], match[2]-1, match[3], match[4]||0, match[5]||0, match[6]||0, match[7]||0);
      }

      return {
        get: function(uri) { return request('GET', uri); },
        post: function(uri, data) { return request('POST', uri, data); },
        put: function(uri, data) { return request('PUT', uri, data); },
        delete: function(uri) { return request('DELETE', uri); },
        date2string: date2string,
        string2date: string2date
      };
    }]);
})(angular);
