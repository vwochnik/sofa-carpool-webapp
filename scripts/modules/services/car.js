/*!
 * car.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').factory('spCar', ['$rootScope', '$q', 'spIdentity', 'spBase',
    function($rootScope, $q, spIdentity, spBase) {
      function getCars() {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          spBase.get('/getCarsFromUser/'+id).then(function(response) {
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function addCar(car) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var carData = ng.extend({userId: id}, car);

          spBase.post('/addCar', carData).then(function(response) {
            var obj = ng.extend({carId: response.carId}, car);
            deferred.resolve(obj);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function updateCar(car) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var carData = ng.extend({}, car);

          spBase.post('/updateCar', carData).then(function(response) {
            var obj = ng.extend({carId: response.carId}, car);
            deferred.resolve(obj);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function deleteCar(carId) {
        var deferred = $q.defer();

        spBase.post('/removeCar', {carId: carId}).then(function(/*response*/) {
          deferred.resolve();
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      return {
        getCars: getCars,
        addCar: addCar,
        updateCar: updateCar,
        deleteCar: deleteCar
      };
    }]);
})(angular);
