/*!
 * address.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng, google) {
  'use strict';

  ng.module('spApp').factory('spAddress', ['$q', 'spIdentity', 'spBase',
    function($q, spIdentity, spBase) {

      var geocoder = new google.maps.Geocoder();

      function getHomeAddress() {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          spBase.get('/getHomeAddressFromUser/'+id).then(function(response) {
            deferred.resolve(response);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function updateHomeAddress(homeAddress) {
        var deferred = $q.defer();

        spIdentity.getID().then(function(id) {
          var homeAddressData = ng.extend({userId: id}, homeAddress);
          spBase.post('/updateUserHomeAddress', homeAddressData).then(function(/*response*/) {
            //TODO: use actual response
            deferred.resolve(homeAddress);
          }, function(reason) {
            deferred.reject(reason);
          });
        }, function(reason) {
          deferred.reject(reason);
        });

        return deferred.promise;
      }

      function stringifyHomeAddress(address) {
        var result = address.street || '';
        result += ((address.postcode || address.city) && result) && ', ' || '';
        result += address.postcode || '';
        result += (address.city && address.postcode) && ' ' || '';
        result += address.city || '';
        result += (address.country && result) && ', ' || '';
        result += address.country || '';
        return result;
      }

      function validateAddress(address) {
        var deferred = $q.defer();

        geocoder.geocode({address: address}, function(results, status) {
          if (status === "OK") {
            var valid = false;
            for (var i = 0; i < results.length; i++) {
              var isstreet = false;
              for (var j = 0; j < results[i].types.length; j++) {
                if (results[i].types[j] === "street_address")
                  isstreet = true;
              }
              if (isstreet) {
                valid = true;
                break;
              }
            }
            deferred.resolve(valid);
          } else if (status === "ZERO_RESULTS") {
            deferred.resolve(false);
          } else if (status === "ERROR") {
            deferred.reject("Google Maps API connection error.");
          } else {
            deferred.reject("Google Maps API unknown error.");
          }
        });

        return deferred.promise;
      }

      return {
        getHomeAddress: getHomeAddress,
        updateHomeAddress: updateHomeAddress,
        stringifyHomeAddress: stringifyHomeAddress,
        validateAddress: validateAddress
      };
    }]);
})(angular, google);
