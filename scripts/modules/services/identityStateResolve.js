/*!
 * identityStateResolve.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  var sRedirect;

  ng.module('spApp').factory('spIdentityStateResolve', ['$q', '$state', 'spIdentity',
    function($q, $state, spIdentity) {

      function identityResolved() {
        var deferred = $q.defer();

        spIdentity.getIdentity().then(function(identity) {
          if (ng.isDefined(identity))
            deferred.resolve();
          else
            deferred.reject();
        }, function() {
          deferred.reject();
        });

        return deferred.promise;
      }

      function resolveRestricted() {
        var deferred = $q.defer();

        identityResolved().then(function() {
          deferred.resolve();
        }, function() {
          deferred.reject();
          sRedirect = 'login';
        });

        return deferred.promise;
      }

      function resolveUnrestricted() {
        var deferred = $q.defer();

        identityResolved().then(function() {
          deferred.reject();
          sRedirect = 'main.index';
        }, function() {
          deferred.resolve();
        });

        return deferred.promise;
      }

      return {
        resolveRestricted: resolveRestricted,
        resolveUnrestricted: resolveUnrestricted
      };
    }])
  .run(['$rootScope', '$state', 'spAlert',
    function($rootScope, $state, spAlert) {
      $rootScope.$on('$stateChangeError', function(event /*, toState, toParams, fromState, fromParams, error*/) {
        if (ng.isDefined(sRedirect)) {
          event.preventDefault();
          $state.go(sRedirect);
          sRedirect = undefined;
        } else {
          spAlert.addAlert('danger', 'State change unsuccessful.');
        }
      });
    }
  ]);
})(angular);
