/*!
 * filters.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').filter('listingSummary',
    function() {
      return function(listing) {
        if (listing.direction === "ARRIVAL")
          return "Arrival from " + listing.location;
        if (listing.direction === "DEPARTURE")
          return "Departure to " + listing.location;
        else
          return "Unknown";
      };
    });

  ng.module('spApp').filter('listingStatus',
    function() {
      return function(listingStatus) {
        switch (listingStatus) {
        case 0: return 'Unprocessed';
        case 1: return 'Open';
        case 2: return 'Closed';
        default: return 'Unknown';
        }
      };
    });

  ng.module('spApp').filter('matchStatus',
    function() {
      return function(matchStatus) {
        switch (matchStatus) {
        case 1: return 'Assigned to user';
        case 2: return 'Accepted by me';
        case 3: return 'Accepted by other';
        case 4: return 'Accepted by both';
        case 5: return 'Declined';
        default: return 'Unknown';
        }
      };
    });

  ng.module('spApp').filter('matchColor',
    function() {
      return function(compatibility) {
        var r, g, b;
        r = 63 + (1.0-compatibility) * 192;
        g = 63 + compatibility * 192;
        b = 0;
        return 'rgb('+Math.round(r)+','+Math.round(g)+','+b+')';
      };
    });

  ng.module('spApp').filter('carLabel',
    function() {
      return function(car) {
        return car.producer
             + (car.model && ((car.producer && ' ' || '') + car.model) || '')
             + (car.color && (((car.producer || car.model) && ', ' || '') + car.color) || '');
      };
    });
})(angular);
