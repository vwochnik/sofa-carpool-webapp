/*!
 * config.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp', ['ngAnimate', 'ui.router', 'ui.bootstrap', 'ngStorage', 'angularMoment', 'spPartials']).config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.hashPrefix('');
      $urlRouterProvider.otherwise('/');
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'partials/login.html',
          resolve: {
            unrestricted: ['spIdentityStateResolve', function(spIdentityStateResolve) {
              return spIdentityStateResolve.resolveUnrestricted();
            }]
          },
          controller: 'spLoginController'
        })
        .state('main', {
          templateUrl: 'partials/main.html',
          controller: 'spMainController',
          resolve: {
            restricted: ['spIdentityStateResolve', function(spIdentityStateResolve) {
              return spIdentityStateResolve.resolveRestricted();
            }]
          },
          abstract: true
        })
        .state('main.intro', {
          url: '/intro',
          templateUrl: 'partials/intro.html',
          controller: 'spIntroController'
        })
        .state('main.dashboard', {
          url: '/',
          templateUrl: 'partials/dashboard.html',
          controller: 'spDashboardController',
          resolve: {
            User: ['spUser', function(spUser) {
              return spUser.getUser();
            }]
          },
          data: {title: 'Dashboard'}
        })
        .state('main.matches', {
          url: '/matches',
          templateUrl: 'partials/matches.html',
          controller: 'spMatchesController',
          data: {title: 'My Matches'},
          resolve: {
            Matches: ['$q', 'spMatch', 'spListing', function($q, spMatch, spListing) {
              var deferred = $q.defer();
              spMatch.getAcceptedMatches().then(function(response) {
                var remaining = response.length;
                if (remaining === 0) {
                  deferred.resolve(response);
                } else {
                  ng.forEach(response, function(value) {
                    if (remaining > 0) {
                      spListing.getListing(value.listingOne).then(function(response2) {
                        value.$listing = response2;
                        if (--remaining === 0)
                          deferred.resolve(response);
                      }, function(reason) {
                        deferred.reject(reason);
                        remaining = 0;
                      });
                      }
                  });
                }
              }, function(reason) {
                deferred.reject(reason);
              });
              return deferred.promise;
            }]
          },
        })
        .state('main.listings', {
          url: '/listings',
          templateUrl: 'partials/listings.html',
          controller: 'spListingsController',
          resolve: {
            Listings: ['spListing', function(spListing) {
              return spListing.getListings();
            }],
            Cars: ['spCar', function(spCar) {
              return spCar.getCars();
            }],
            HomeAddress: ['spAddress', function(spAddress) {
              return spAddress.getHomeAddress();
            }]
          },
          data: {title: 'My Listings'}
        })
        .state('main.cars', {
          url: '/cars',
          templateUrl: 'partials/cars.html',
          controller: 'spCarsController',
          resolve: {
            Cars: ['spCar', function(spCar) {
              return spCar.getCars();
            }]
          },
          data: {title: 'My Cars'}
        })
        .state('main.profile', {
          url: '/profile',
          templateUrl: 'partials/profile.html',
          controller: 'spProfileController',
          resolve: {
            User: ['spUser', function(spUser) {
              return spUser.getUser();
            }],
            HomeAddress: ['spAddress', function(spAddress) {
              return spAddress.getHomeAddress();
            }]
          },
          data: {title: 'My Profile'}
        });
    }]);
})(angular);
