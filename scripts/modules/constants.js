/*!
 * constants.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').constant('spConstants', {
    location: 'Fontys Venlo',
    //apiUrl: 'http://145.93.248.231:8080/rest/WebService',
    apiUrl: 'http://fantony.net:8181/rest/WebService',
    //apiUrl: '/rest/WebService',
    datepickerOptions: {
      formatYear: 'yy',
      startingDay: 1
    },
    arrivalTimes: [
      {h: 8, m: 45, l: "08:45"},
      {h: 9, m: 35, l: "09:35"},
      {h: 10, m: 40, l: "10:40"},
      {h: 11, m: 30, l: "11:30"},
      {h: 12, m: 20, l: "12:20"},
      {h: 13, m: 10, l: "13:10"},
      {h: 14, m: 0, l: "14:00"},
      {h: 15, m: 5, l: "15:05"},
      {h: 15, m: 55, l: "15:55"},
      {h: 16, m: 45, l: "16:45"}
    ],
    departureTimes: [
      {h: 9, m: 30, l: "09:30"},
      {h: 10, m: 20, l: "10:20"},
      {h: 11, m: 25, l: "11:25"},
      {h: 12, m: 15, l: "12:15"},
      {h: 13, m: 5, l: "13:05"},
      {h: 13, m: 55, l: "13:55"},
      {h: 14, m: 45, l: "14:45"},
      {h: 15, m: 50, l: "15:50"},
      {h: 16, m: 40, l: "16:40"},
      {h: 17, m: 30, l: "17:30"}
    ],
    compatibilityColor: function(compatibility) {
      var r, g, b;
      r = 63 + (1.0-compatibility) * 192;
      g = 63 + compatibility * 192;
      b = 0;
      return 'rgb('+Math.round(r)+','+Math.round(g)+','+b+')';
    }
  }).run(['$rootScope', 'spConstants', function($rootScope, spConstants) {
    $rootScope.Constants = spConstants;
  }]);
})(angular);
