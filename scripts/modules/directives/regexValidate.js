/*!
 * regexValidate.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').directive('regexValidate', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elem, attr, ctrl) {
        var regex = new RegExp(attr.regexValidate, attr.regexValidateFlags || '');

        ctrl.$parsers.unshift(function(value) {
          var valid = regex.test(value);
          ctrl.$setValidity('regexValidate', valid);
          return valid ? value : undefined;
        });

        ctrl.$formatters.unshift(function(value) {
          ctrl.$setValidity('regexValidate', regex.test(value));
          return value;
        });
      }
    };
  });
})(angular);
