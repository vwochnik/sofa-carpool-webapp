/*!
 * carsController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spCarsController', ['$scope', '$filter', 'spIdentity', 'spAlert', 'spModal', 'spCar', 'Cars',
    function($scope, $filter, spIdentity, spAlert, spModal, spCar, Cars) {
      $scope.showCarForm = false;
      $scope.progressing = false;
      $scope.cars = Cars;

      $scope.newCar = function() {
        if (($scope.showCarForm) || ($scope.progressing))
          return;
        $scope.car = {producer: '', model: '', color: '', consumption: 0.0, seats: 1};
        $scope.showCarForm = true;
      };

      $scope.newCar = function() {
        $scope.car = {producer: '', model: '', color: '', consumption: 0.0, seats: 1};
        $scope.showCarForm = true;
      };

      $scope.editCar = function(car) {
        $scope.car = car;
        $scope.showCarForm = true;
      };

      $scope.clearNewCar = function() {
        $scope.showCarForm = false;
      };

      $scope.submitCar = function() {
        $scope.progressing = true;
        if ($scope.car.carId) {
          spCar.updateCar($scope.car).then(function(car) {
            spAlert.addAlert('success', 'Car edited successfully.');
            $scope.showCarForm = false;
            ng.extend($scope.car, car);
          }, function(reason) {
            spAlert.addAlert('danger', reason);
          }).finally(function() {
            $scope.progressing = false;
          });
        } else {
          spCar.addCar($scope.car).then(function(car) {
            spAlert.addAlert('success', 'Car added successfully.');
            $scope.showCarForm = false;
            $scope.cars.push(car);
          }, function(reason) {
            spAlert.addAlert('danger', reason);
          }).finally(function() {
            $scope.progressing = false;
          });
        }
      };

      $scope.deleteCar = function(car) {
        spModal.modalQuestion('Delete Car', 'Do you really want to delete this car?').then(function() {
          $scope.progressing = true;
          spCar.deleteCar(car.carId).then(function() {
            spAlert.addAlert('success', 'Car deleted successfully.');
            $scope.progressing = false;
            $scope.cars = $filter('filter')($scope.cars, {carId: '!'+car.carId});
          }, function(reason) {
            spAlert.addAlert('danger', reason);
            $scope.progressing = false;
          });
        });
      };
    }]);
})(angular);
