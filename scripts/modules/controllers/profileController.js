/*!
 * profileController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spProfileController', ['$scope', 'spAlert', 'spModal', 'spUser', 'spAddress', 'User', 'HomeAddress',
    function($scope, spAlert, spModal, spUser, spAddress, User, HomeAddress) {
      $scope.birthDateOpened = false;
      $scope.progressing = false;
      $scope.user = User;
      $scope.homeAddress = HomeAddress;

      $scope.openBirthDate = function($event) {
        $scope.birthDateOpened = true;
        $event.preventDefault();
        $event.stopPropagation();
      };

      $scope.smokingChanged = function() {
        if ($scope.user.smoking) {
          spModal.modalQuestion('Enable Smoking', 'Do you really want to enable smoking mode?', ['If you enable smoking mode, you will only be matched with other people who have the smoking mode enabled, too.', 'This will significantly reduce your number of matches.']).then(function() {
          }, function() {
            if ($scope.user.smoking)
              $scope.user.smoking = false;
          });
        }
      };

      $scope.submitProfile = function() {
          $scope.progressing = true;
          spUser.updateUser($scope.user).then(function(value) {
            $scope.user = value;
            $scope.progressing = false;
            spAlert.addAlert('success', 'Profile update successful.');
          }, function(reason) {
            $scope.progressing = false;
            spAlert.addAlert('danger', reason);
          });
      };

      $scope.submitHomeAddress = function() {
        $scope.progressing = true;
        spAddress.validateAddress(spAddress.stringifyHomeAddress($scope.homeAddress)).then(function(valid) {
          if (valid) {
            spAddress.updateHomeAddress($scope.homeAddress).then(function(value) {
              $scope.homeAddress = value;
              $scope.progressing = false;
              spAlert.addAlert('success', 'Home address update successful.');
            }, function(reason) {
              $scope.progressing = false;
              spAlert.addAlert('danger', reason);
            });
          } else {
            $scope.progressing = false;
            spAlert.addAlert('danger', "Invalid home address.");
          }
        }, function(reason){
          $scope.progressing = false;
          spAlert.addAlert('danger', reason);
        });
      };
    }]);
})(angular);
