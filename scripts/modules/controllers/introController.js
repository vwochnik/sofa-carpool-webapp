/*!
 * introController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spIntroController', ['$scope', '$state',
    function($scope, $state) {
      $scope.nextClicked = function() {
        $state.go('main.dashboard');
      };
    }]);
})(angular);
