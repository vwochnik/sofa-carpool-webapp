/*!
 * navbarController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function($, ng) {
  'use strict';

  ng.module('spApp').controller('spNavbarController', ['$rootScope', '$scope', '$state',
    function($rootScope, $scope, $state) {
      // set menu items
      $scope.menu = [];
      ng.forEach($state.get(), function(state) {
        if ((state.name.slice(0, 5) === 'main.') && (ng.isDefined(state.data)) && (ng.isDefined(state.data.title)))
          $scope.menu.push({name: state.name, title: state.data.title});
      });

      $scope.menuClick = function() {
        $(".navbar-toggle:visible").click();
      };

      $scope.bare = false;
      $scope.$on('$stateChangeSuccess', function(event, toState) {
        $scope.bare = (toState.name === 'login');
      });
    }]);
})(jQuery, angular);
