/*!
 * mainController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spMainController', ['$scope', '$state',
    function($scope, $state) {
      $scope.tabs = [];
      ng.forEach($state.get(), function(state) {
        if ((state.name.slice(0, 5) === 'main.') && (ng.isDefined(state.data)) && (ng.isDefined(state.data.title)))
          $scope.tabs.push({name: state.name, title: state.data.title});
      });
    }]);
})(angular);
