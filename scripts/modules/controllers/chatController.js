/*!
 * chatController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function($, ng) {
  'use strict';

  ng.module('spApp').controller('spChatController', ['$sce', '$timeout', '$scope', '$modalInstance', 'spMessage', 'Match', 'Listing', 'Messages',
    function($sce, $timeout, $scope, $modalInstance, spMessage, Match, Listing, Messages) {
      var timeoutPromise;

      function sceMessage(message) {
        message.$message = $sce.trustAsHtml(message.message);
      }

      function scrollToBottom() {
        $('.chat-overflow').each(function() {
          $(this).scrollTo(this.scrollHeight);
        });
      }

      function cancelRefresh() {
        if (ng.isDefined(timeoutPromise))
          $timeout.cancel(timeoutPromise);
      }

      function timedRefresh() {
        cancelRefresh();
        timeoutPromise = $timeout(function() {
          spMessage.getMessagesForMatch(Match).then(function(messages) {
            $scope.messages = ng.forEach(messages, sceMessage);
            scrollToBottom();
          }).finally(function() {
            timeoutPromise = undefined;
            timedRefresh();
          });
        }, 5000);
      }

      $scope.$on('$destroy', cancelRefresh);

      $scope.match = Match;
      $scope.listing = Listing;
      $scope.messages = ng.forEach(Messages, sceMessage);
      $scope.progressing = false;

      $scope.close = function() {
        $modalInstance.close(true);
      };

      $scope.sendMessage = function() {
        $scope.progressing = true;
        spMessage.sendMessage(Match, $scope.message).then(function() {
          $scope.message = '';
          //TODO: won't work atm b/c server doesn't immediately return sent message upon next request
          //timedRefresh();
        }).finally(function() {
          $scope.progressing = false;
        });
      };

      timedRefresh();
      $timeout(scrollToBottom, 0);
    }]);
})(jQuery, angular);
