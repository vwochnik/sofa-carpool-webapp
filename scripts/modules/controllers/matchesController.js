/*!
 * matchesController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spMatchesController', ['$modal', '$scope', 'spAlert', 'spModal', 'spMatch', 'Matches',
    function($modal, $scope, spAlert, spModal, spMatch, Matches) {
      $scope.matches = Matches;
      $scope.progressing = false;

      $scope.openChat = function(match) {
        $scope.progressing = true;
        $modal.open({
          templateUrl: 'partials/chat.html',
          controller: 'spChatController',
          resolve: {
            Messages: ['spMessage', function(spMessage) {
              return spMessage.getMessagesForMatch(match);
            }],
            Match: function() { return match; },
            Listing: function() { return match.$listing; }
          }
        }).result.finally(function() {
          $scope.progressing = false;
        });
      };

      $scope.showDetail = function(match) {
        match.$showDetail = !match.$showDetail;
      };

      $scope.declineMatch = function(match) {
        spModal.modalQuestion('Decline Match', 'Do you really want to decline this match?').then(function() {
          $scope.progressing = true;
          spMatch.declineMatch(match).then(function(response) {
            match = response;
            if (match.status !== 4)
              $scope.matches.remove($scope.matches.indexOf(match));
            spAlert.addAlert('success', 'Match declined successfully.');
            $scope.progressing = false;
          }, function(reason) {
            spAlert.addAlert('danger', reason);
            $scope.progressing = false;
          });
        });
      };
    }]);
})(angular);
