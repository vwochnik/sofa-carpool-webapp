/*!
 * modalAlertController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spModalAlertController', ['$scope', '$modalInstance', 'heading', 'lead', 'notes',
    function($scope, $modalInstance, heading, lead, notes) {
      $scope.heading = heading;
      $scope.lead = lead;
      $scope.notes = notes;

      $scope.dismiss = function() {
        $modalInstance.close();
      };
    }]);
})(angular);
