/*!
 * userActionController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function($, ng) {
  'use strict';

  ng.module('spApp').controller('spUserActionController', ['$scope', '$state', 'spAlert', 'spIdentity', 'spUser',
    function($scope, $state, spAlert, spIdentity, spUser) {
      function updateDisplayName() {
        if (ng.isDefined($scope.user))
          $scope.displayName = $scope.user.firstName;
        else
          $scope.displayName = '';
      }

      function updateUser(user) {
        $scope.user = user;
        updateDisplayName();
      }

      function updateIdentity(identity) {
        $scope.loggedIn = ng.isDefined(identity);
        $scope.identity = identity;
        updateDisplayName();
      }

      $scope.$on('identity:updated', function(event, identity) {
        updateIdentity(identity);
      });

      spIdentity.getIdentity().then(function(identity) {
        updateIdentity(identity);
      });

      $scope.$on('user:updated', function(event, user) {
        updateUser(user);
      });

      spUser.getUser().then(function(user) {
        updateUser(user);
      });

      $scope.logOut = function() {
        $(".navbar-toggle:visible").click();
        spIdentity.logOut().then(function(value) {
          if (!ng.isDefined(value)) {
            spAlert.addAlert('success', 'You are now logged out!');
            $state.go('login');
          }
        }, function(reason) {
          spAlert.addAlert('danger', reason);
        });
      };
    }]);
})(jQuery, angular);
