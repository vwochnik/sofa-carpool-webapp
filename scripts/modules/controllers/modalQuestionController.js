/*!
 * modalQuestionController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spModalQuestionController', ['$scope', '$modalInstance', 'heading', 'lead', 'notes',
    function($scope, $modalInstance, heading, lead, notes) {
      $scope.heading = heading;
      $scope.lead = lead;
      $scope.notes = notes;

      $scope.confirm = function() {
        $modalInstance.close(true);
      };

      $scope.deny = function() {
        $modalInstance.close(false);
      };
    }]);
})(angular);
