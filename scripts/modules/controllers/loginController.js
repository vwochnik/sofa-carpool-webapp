/*!
 * loginController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function($, ng) {
  'use strict';

  ng.module('spApp').controller('spLoginController', ['$rootScope', '$scope', '$state', '$timeout', 'spAlert', 'spIdentity',
    function($rootScope, $scope, $state, $timeout, spAlert, spIdentity) {
      $scope.remember = true;
      $scope.progressing = false;
      $scope.failedLogin = false;
      $scope.suffix = "@student.fontys.nl";

      $scope.change = function() {
        $scope.failedLogin = false;
      };

      $scope.submit = function() {
        $scope.progressing = true;
        spIdentity.logIn($scope.pcn+$scope.suffix, $scope.password, $scope.remember).then(function(identity) {
          spAlert.addAlert('success', 'Logged in successfully.');
          if (identity.firstLogin === true) {
            identity.firstLogin = false;
            $state.go('main.intro');
          } else {
            $state.go('main.dashboard');
          }
        }, function(reason) {
          $scope.failedLogin = true;
          spAlert.addAlert('danger', reason);
          $timeout(function() {
            $scope.failedLogin = false;
          }, 5000);
        }).finally(function() {
          $scope.progressing = false;
        });
      };

      $.introHeight("#login");
      $rootScope.introPage = true;
      $scope.$on('$destroy', function() {
        $.introHeight.unset();
        $rootScope.introPage = false;
      });
    }]);
})(jQuery, angular);
