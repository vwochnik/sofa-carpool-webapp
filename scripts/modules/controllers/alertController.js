/*!
 * alertController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spAlertController', ['$scope', 'spAlert',
    function($scope, spAlert) {
      $scope.alerts = spAlert.getAlerts();

      $scope.close = function(alert) {
          spAlert.removeAlert(alert);
      };
    }]);
})(angular);
