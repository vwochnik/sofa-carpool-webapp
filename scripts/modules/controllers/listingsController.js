/*!
 * listingsController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spListingsController', ['$q', '$scope', '$filter', 'spConstants', 'spIdentity', 'spAlert', 'spModal', 'spListing', 'spMatch', 'spAddress', 'Listings', 'Cars', 'HomeAddress',
    function($q, $scope, $filter, spConstants, spIdentity, spAlert, spModal, spListing, spMatch, spAddress, Listings, Cars, HomeAddress) {
      $scope.showNewListing = false;
      $scope.progressing = false;
      $scope.listings = Listings;
      $scope.cars = Cars;

      $scope.toggleListingDetail = function(listing) {
        if (listing.$showDetail) {
          listing.$showDetail = false;
        } else if (!ng.isDefined(listing.$matches)) {
          spMatch.getMatchesForListing(listing.listingId).then(function(response) {
            listing.$matches = response;
            listing.$showDetail = true;
          });
        } else {
          listing.$showDetail = true;
        }
      };

      $scope.newListing = function() {
          if (($scope.showNewListing) || ($scope.progressing))
            return;

          $scope.direction = 'ARRIVAL';
          $scope.driver = false;
          $scope.car = $scope.cars[0];
          $scope.minDate = new Date();
          $scope.dateOpened = false;
          $scope.location = "";
          $scope.date = new Date();
          $scope.arrivalTime = spConstants.arrivalTimes[0];
          $scope.departureTime = spConstants.departureTimes[0];
          $scope.showNewListing = true;
      };

      $scope.setHomeAddress = function() {
        var newloc = spAddress.stringifyHomeAddress(HomeAddress);
        $scope.location = newloc || $scope.location;
      };

      $scope.driverChanged = function() {
        if (($scope.driver) && ($scope.cars.length === 0)) {
          spModal.modalAlert('No Cars Found', 'There are no cars for selection in the database.', ['If you want to enable driving, you first have to add a car.']).then(function() {
            $scope.driver = false;
          });
        }
      };

      $scope.clearNewListing = function() {
        $scope.showNewListing = false;
      };

      $scope.submitNewListing = function() {
        function listing(direction, time) {
          spListing.addListing({
            direction: direction,
            location: $scope.location,
            carId: $scope.driver ? $scope.car.carId : -1,
            time: new Date($scope.date.getFullYear(), $scope.date.getMonth(), $scope.date.getDate(), time.h, time.m, 0, 0)
          }).then(function(listing) {
            $scope.listings.push(listing);
          });
        }

        $scope.progressing = true;
        spAddress.validateAddress($scope.location).then(function(valid) {
          if (valid) {
            var list = [];
            if ($scope.direction !== "DEPARTURE")
              list.push(listing("ARRIVAL", $scope.arrivalTime));
            if ($scope.direction !== "ARRIVAL")
              list.push(listing("DEPARTURE", $scope.departureTime));
            $q.all(list).then(function() {
              $scope.showNewListing = false;
              spAlert.addAlert('success', 'Listing added successfully');
            }, function(reason) {
              spAlert.addAlert('danger', reason);
            }).finally(function() {
              $scope.progressing = false;
            });
          } else {
            $scope.progressing = false;
            spAlert.addAlert('danger', "Invalid location entered.");
          }
        }, function(reason) {
          $scope.progressing = false;
          spAlert.addAlert('danger', reason);
        });
      };

      $scope.deleteListing = function(listingId) {
        spModal.modalQuestion('Delete Listing', 'Do you really want to delete this listing?').then(function() {
          $scope.progressing = true;
          spListing.deleteListing(listingId).then(function() {
            spAlert.addAlert('success', 'Listing deleted successfully.');
            $scope.progressing = false;
            $scope.listings = $filter('filter')($scope.listings, {listingId: '!'+listingId});
          }, function(reason) {
            spAlert.addAlert('danger', reason);
            $scope.progressing = false;
          });
        });
      };

      $scope.acceptMatch = function(match) {
        $scope.progressing = true;
        spMatch.acceptMatch(match).then(function(response) {
          match = response;
          spAlert.addAlert('success', 'Match accepted successfully.');
          $scope.progressing = false;
        }, function(reason) {
          spAlert.addAlert('danger', reason);
          $scope.progressing = false;
        });
      };

      $scope.declineMatch = function(match) {
        spModal.modalQuestion('Decline Match', 'Do you really want to decline this match?').then(function() {
          $scope.progressing = true;
          spMatch.declineMatch(match).then(function(response) {
            match = response;
            spAlert.addAlert('success', 'Match declined successfully.');
            $scope.progressing = false;
          }, function(reason) {
            spAlert.addAlert('danger', reason);
            $scope.progressing = false;
          });
        });
      };

      $scope.openDate = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.dateOpened = true;
      };
    }]);
})(angular);
