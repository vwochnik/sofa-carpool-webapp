/*!
 * dashboardController.js
 * Copyright 2014 SmartPooling Team. All rights reserved.
 */

(function(ng) {
  'use strict';

  ng.module('spApp').controller('spDashboardController', ['$scope', 'User',
    function($scope, User) {
      $scope.user = User;
    }]);
})(angular);
