(function($, wnd) {
  'use strict';

  var $elem = undefined;

  function refreshHeight() {
    var last, curr, top;
    last = $elem.height();
    curr = $elem.height($(wnd).innerHeight()).height();
    top = $(wnd).scrollTop();
    if (top > 0)
      $(wnd).scrollTop(Math.max(0, top+curr-last));
  }

  function introHeight(id) {
    if (typeof $elem !== 'undefined')
      return false;

    var $e = $(id).children(".wrapper");
    if ($e.length == 0)
      return false;
    $elem = $e.first();
    $(wnd).on('resize.introHeight', refreshHeight);
    refreshHeight();
    return true;
  }

  function unset() {
    if (typeof $elem === 'undefined')
      return;
    $(wnd).off('resize.introHeight');
    $elem = undefined;
  }

  $.introHeight = $.extend(introHeight, {
    unset: unset
  });
})(jQuery, window);
