(function($) {
    function checkScrolledTop(isMobile) {
        var top = $(window).width()/5.0,
            scroll = $(window).scrollTop();
        if (((!isMobile) && (scroll <= top)) || (scroll == 0))
            $(document.body).addClass("scrolled-top");
        else if (scroll > top)
            $(document.body).removeClass("scrolled-top");
    }

    $(function() {
      if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          $(window).on('scroll resize', function(){checkScrolledTop(false);});
          checkScrolledTop(false);
      } else {
          $(window).on('scroll resize touchmove', function(){checkScrolledTop(true);});
          checkScrolledTop(true);
      }

      if ($(".landing-page").length) {
        // activate intro height
        $.introHeight("#intro");

        // activate scrollspy and update hash while scrolling
        $(document.body).scrollspy({target: ".navbar"});

        // activate smooth scrolling
        $.localScroll({queue: true, duration: 1000});
        $('.landing-page .navbar-brand').click(function(e) {
            $.scrollTo(0, 0, {duration: 1000, queue: true});
            return false;
        });
      }
    });
})(jQuery);
