SoFa Carpooling Smartpooling Web Application
============================================

This web application can be installed by following the steps below:

0. If you haven't already, check out the SVN repository with Git by running `git svn clone https://www.fontysvenlo.org/svn/2014/sofa/g2/trunk/04_implementation/webapp sofa-carpool-webapp`. This is necessary, because of the `.gitignore` file. Use the `--username` parameter if you have to. After you have cloned the repository, run `cd sofa-carpool-webapp`.

1. Install NodeJS with the Node Package Manager.

2. Execute `npm install -g grunt-cli bower` with root privileges if need be.

3. Run `npm install` and `bower install` in the project directory.

4. Run `grunt` to build the application, `grunt develop` to run a test environment and finally `grunt deploy` to deploy the application.
